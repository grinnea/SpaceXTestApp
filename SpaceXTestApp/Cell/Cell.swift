//
//  Cell.swift
//  SpaceXTestApp
//
//  Created by Grinea on 20.10.2022.
//

import UIKit

class Cell: UITableViewCell {
    
    @IBOutlet weak var imageLaunch: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var imageStar: UIImageView!
    
    var favorit = false
    var id = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 10.0
        layer.masksToBounds = true
    }
    
    @IBAction func starButtonAction(_ sender: Any) {
        if favorit {
            favorit = false
            favoriteList = favoriteList.filter { $0 != id}
            imageStar.image = UIImage(systemName: "star", withConfiguration: .none)
        }else{
            favorit = true
            favoriteList.append(id)
            imageStar.image = UIImage(systemName: "star.fill", withConfiguration: .none)
        }
        UserDefaults.standard.set(favoriteList, forKey: "favoriteList")
    }
    
}
