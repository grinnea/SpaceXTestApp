//
//  AlertManager.swift
//  SpaceXTestApp
//
//  Created by Grinea on 20.10.2022.
//

import UIKit

// MARK: - Alert

class AlertManager {
    class func showOkAlert(title: String?, message: String) {
        let rootViewController = UIApplication
        .shared
        .connectedScenes
        .flatMap { ($0 as? UIWindowScene)?.windows ?? [] }
        .first { $0.isKeyWindow }?.rootViewController
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default)
        alert.addAction(ok)
        rootViewController?.present(alert, animated: true)
    }
}
