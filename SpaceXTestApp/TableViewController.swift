//
//  TableViewController.swift
//  SpaceXTestApp
//
//  Created by Grinea on 20.10.2022.
//

import UIKit

class TableViewController: UITableViewController {

    @IBOutlet weak var favoritListButton: UIBarButtonItem!
    
    var launches: [Launch] = []
    var tableViewData: [Launch] = []
    var favorite = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "Cell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        NetworkManager.fetchDataFromNetwork(completion: {launches, error  in
            self.launches = launches ?? []
            self.tableViewData = launches ?? []
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if favoriteList.count == 0 && favorite {
            AlertManager.showOkAlert(title: "Warning",
                                     message: "Favorite list is empty")
            favoriteListShow()
        }else if favorite {
            tableViewData = launches.filter({ favoriteList.contains($0.id ?? "") })
        }
        self.tableView.reloadData()
    }
    
    @IBAction func favoritListAction(_ sender: Any) {
        if favoriteList.count == 0 && !favorite {
            AlertManager.showOkAlert(title: "Warning",
                                     message: "Favorite list is empty")
        }else{
            favoriteListShow(favorite)
        }
    }
    
    func favoriteListShow(_ on: Bool = true) {
        if on {
            favoritListButton.image = UIImage(systemName: "star.square.on.square", withConfiguration: .none)
            favorite = false
            tableViewData = launches
        }else{
            favoritListButton.image = UIImage(systemName: "star.square.on.square.fill", withConfiguration: .none)
            favorite = true
            tableViewData = launches.filter({ favoriteList.contains($0.id ?? "") })
        }
        self.tableView.reloadData()
    }
    
    // MARK: - TableView

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! Cell
        let row = indexPath.row
        cell.name.text = tableViewData[row].name
        cell.date.text = tableViewData[row].date_utc?.toDateFormat()
        cell.id = tableViewData[row].id ?? ""
        cell.imageLaunch.load(urlString: tableViewData[row].links.patch.small ?? "")
        cell.favorit = favoriteList.contains(tableViewData[row].id ?? "")
        cell.imageStar.image = UIImage(systemName: cell.favorit ? "star.fill" : "star", withConfiguration: .none)
    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDiscription", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDiscription" {
            let vc = segue.destination as? DiscriptionViewController
            vc?.launches = tableViewData
            vc?.indexRow = tableView.indexPathForSelectedRow?.row ?? 0
        }
    }
    
}
