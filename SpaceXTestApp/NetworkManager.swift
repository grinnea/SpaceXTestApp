//
//  NetworkManager.swift
//  SpaceXTestApp
//
//  Created by Grinea on 20.10.2022.
//

import Foundation

// MARK: - Get Data from Network

class NetworkManager {
    class func fetchDataFromNetwork(completion: @escaping ([Launch]?, Error?) -> ()) {
        guard let url = URL(string: "https://api.spacexdata.com/v5/launches") else { return }
        URLCache.shared.removeAllCachedResponses()
        URLSession.shared.dataTask(with: url) { data, response, error in
            do {
                    guard let data = data,
                          let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode),
                          error == nil else {
                        completion(nil, error)
                        DispatchQueue.main.async {
                            AlertManager.showOkAlert(title: "Error",
                                                     message: "Data could not be retrieved")
                        }
                        return
                    }
                let launches = try JSONDecoder().decode([Launch].self, from: data)
                completion(launches, nil)
            } catch {
                completion(nil, error)
                DispatchQueue.main.async {
                    AlertManager.showOkAlert(title: "Error",
                                             message: "Data could not be retrieved")
                }
            }
        }.resume()
    }
    
    class func getRocket(id: String, completion: @escaping (Rocket?, Error?) -> ()) {
        guard let url = URL(string: "https://api.spacexdata.com/v4/rockets/\(id)") else { return }
        URLCache.shared.removeAllCachedResponses()
        URLSession.shared.dataTask(with: url) { data, response, error in
            do {
                    guard let data = data,
                          let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode),
                          error == nil else {
                        completion(nil, error)
                        DispatchQueue.main.async {
                            AlertManager.showOkAlert(title: "Error",
                                                     message: "Data could not be retrieved")
                        }
                        return
                    }
                let launches = try JSONDecoder().decode(Rocket.self, from: data)
                completion(launches, nil)
            } catch {
                completion(nil, error)
                DispatchQueue.main.async {
                    AlertManager.showOkAlert(title: "Error",
                                             message: "Data could not be retrieved")
                }
            }
        }.resume()
    }

    class func getPayload(id: String, completion: @escaping (Payload?, Error?) -> ()) {
        guard let url = URL(string: "https://api.spacexdata.com/v4/payloads/\(id)") else { return }
        URLCache.shared.removeAllCachedResponses()
        URLSession.shared.dataTask(with: url) { data, response, error in
            do {
                    guard let data = data,
                          let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode),
                          error == nil else {
                        completion(nil, error)
                        DispatchQueue.main.async {
                            AlertManager.showOkAlert(title: "Error",
                                                     message: "Data could not be retrieved")
                        }
                        return
                    }
                let launches = try JSONDecoder().decode(Payload.self, from: data)
                completion(launches, nil)
            } catch {
                completion(nil, error)
                DispatchQueue.main.async {
                    AlertManager.showOkAlert(title: "Error",
                                             message: "Data could not be retrieved")
                }
            }
        }.resume()
    }
}
