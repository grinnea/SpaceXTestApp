//
//  StringExtension.swift
//  SpaceXTestApp
//
//  Created by Grinea on 22.10.2022.
//2020-05-30T15:22:00-04:00

import Foundation

extension String {
    
    func toDateFormat() -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let outputFormatter = DateFormatter()
        outputFormatter.locale = Locale(identifier: "en_US")
        outputFormatter.dateFormat = "MMMM dd, yyyy"
        
        guard let date = formatter.date(from: self) else {
            let date = Date()
            return outputFormatter.string(from: date)
        }
        
        return outputFormatter.string(from: date)
    }
}
