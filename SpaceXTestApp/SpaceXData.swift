//
//  SpaceXData.swift
//  SpaceXTestApp
//
//  Created by Grinea on 20.10.2022.
//

import Foundation

// MARK: - Data

struct Launch: Codable {
    let id: String?
    let name: String?
    let date_utc: String?
    let links: Links
    let details: String?
    let rocket: String?
    let payloads: [String]?
}

struct Links: Codable {
    let patch: Patch
    let webcast: String?
    let wikipedia: String?
}

struct Patch: Codable {
    let small: String?
}

struct Rocket: Codable {
    let name: String?
}

struct Payload: Codable {
    let mass_kg: Int?
}

var favoriteList = (UserDefaults.standard.array(forKey: "favoriteList") as? [String]) ?? [String]()
