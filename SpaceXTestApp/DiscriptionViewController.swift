//
//  DiscriptionViewController.swift
//  SpaceXTestApp
//
//  Created by Grinea on 21.10.2022.
//

import UIKit
import WebKit

class DiscriptionViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var dateLaunch: UILabel!
    @IBOutlet weak var textViewLunch: UITextView!
    @IBOutlet weak var rocketName: UILabel!
    @IBOutlet weak var payloadMass: UILabel!
    @IBOutlet weak var wikipedia: UIButton!
    @IBOutlet weak var starButton: UIBarButtonItem!
    
    var launches = [Launch]()
    var indexRow = 0
    var favorit = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareDiscriptin()
    }
    
    @IBAction func wikipediaButton(_ sender: Any) {
        if let url = URL(string: launches[indexRow].links.wikipedia ?? "") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func starButton(_ sender: Any) {
        if favorit {
            favorit = false
            favoriteList = favoriteList.filter { $0 != launches[indexRow].id ?? "" }
        }else{
            favorit = true
            favoriteList.append(launches[indexRow].id ?? "")
        }
        UserDefaults.standard.set(favoriteList, forKey: "favoriteList")
        favoritInfo()
    }
    
    func prepareDiscriptin() {
        let launch = launches[indexRow]
        getVideo(url: launch.links.webcast ?? "")
        textViewLunch.text = launch.details
        dateLaunch.text = launch.date_utc?.toDateFormat()
        favorit = favoriteList.contains(launch.id ?? "")
        NetworkManager.getRocket(id: launch.rocket ?? "", completion: {rocket, error  in
            DispatchQueue.main.async {
                self.rocketName.text = "Rocket name: " + (rocket?.name ?? "")
            }
        })
        NetworkManager.getPayload(id: launch.payloads?.first ?? "", completion: {payload, error  in
            DispatchQueue.main.async {
                self.payloadMass.text = "Payload mass: \(payload?.mass_kg ?? 0) kg"
            }
        })
        favoritInfo()
    }
    
    func favoritInfo() {
        if favorit {
            starButton.image = UIImage(systemName: "star.fill", withConfiguration: .none)
        }else{
            starButton.image = UIImage(systemName: "star", withConfiguration: .none)
        }
    }
    
    func getVideo(url: String) {
        webView.configuration.allowsInlineMediaPlayback = true
        guard let url = URL(string: url) else { return }
            webView.load(URLRequest(url: url))
    }
}
